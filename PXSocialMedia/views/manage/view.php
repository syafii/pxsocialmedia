<?php
/* @var $this Social Media UserController */
/* @var $model Social Media User */
$title  = Yii::app()->controller->module->title;
$this->breadcrumbs=array(
	$title=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List '.$title, 'url'=>array('index')),
	array('label'=>'Create '.$title, 'url'=>array('create')),
	array('label'=>'Update '.$title, 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete '.$title, 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage '.$title, 'url'=>array('admin')),
);
?>

<h1>View <?php echo $title; ?> #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'user_name',
		'password',
		'email',
		'status',
		'avatar',
		'user_name_base',
		'social_id',
		'registration_type',
		'latitude',
		'longitude',
	),
)); ?>
