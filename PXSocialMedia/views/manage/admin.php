<?php
/* @var $this Social Media UserController */
/* @var $model Social Media User */
$title  = Yii::app()->controller->module->title;
$this->breadcrumbs=array(
	$title=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List '.$title, 'url'=>array('index')),
	array('label'=>'Create '.$title, 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#module-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage <?=$title; ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'module-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_id',
		'user_name',
		'password',
		'email',
		'status',
		/*
		'avatar',
		'user_name_base',
		'social_id',
		'registration_type',
		'latitude',
		'longitude',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
