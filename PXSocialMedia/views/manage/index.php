<?php
/* @var $this Social Media UserController */
/* @var $dataProvider CActiveDataProvider */
$title  = Yii::app()->controller->module->title;
$this->breadcrumbs=array(
	$title,
);

$this->menu=array(
	array('label'=>'Create '.$title, 'url'=>array('create')),
	array('label'=>'Manage '.$title, 'url'=>array('admin')),
);
?>

<h1><?php echo $title; ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
