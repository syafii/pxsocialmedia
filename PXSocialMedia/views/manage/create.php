<?php
/* @var $this Social Media UserController */
/* @var $model Social Media User */
$title  = Yii::app()->controller->module->title;
$this->breadcrumbs=array(
	$title=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List '.$title, 'url'=>array('index')),
	array('label'=>'Manage '.$title, 'url'=>array('admin')),
);
?>

<h1>Create <?php echo $title; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>