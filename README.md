# PXSocialMedia
Yii module that generate API to manage Social Media authorization automatically.

The module will create a table called `pxsocial_user` contains all the authorized users.

## Installation
### To use a module, first download PXSocialMedia Module and place the module directory under modules of the application base directory. Then declare the module ID in the modules property of the application. 
### A module should be configured with initial property values. The usage is very similar to configuring application components. We can use the following application configuration:

/protected/config/main.php
--------------------------
return array(
   ......
   'PXSocialMedia'=>array
       (
           'supports' => array('facebook','twitter', 'instagram', 'email'),
           'api_basicauthorization' => array('user'=>'admin', 'pass'=>'admin'),
           'title' => 'Social User',
       ),
   ......
   'urlManager'=>array
       (
       'urlFormat'=>'path',
       'rules'=>array
           (                   
               array('PXSocialMedia/api/list', 'pattern'=>'PXSocialMedia/api/<model:\w+>', 'verb'=>'GET'),
               array('PXSocialMedia/api/view', 'pattern'=>'PXSocialMedia/api/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
               array('PXSocialMedia/api/update', 'pattern'=>'PXSocialMedia/api/<model:\w+>/<id:\d+>', 'verb'=>'PUT'),
               array('PXSocialMedia/api/delete', 'pattern'=>'PXSocialMedia/api/<model:\w+>/<id:\d+>', 'verb'=>'DELETE'),
               array('PXSocialMedia/api/create', 'pattern'=>'PXSocialMedia/api/<model:\w+>', 'verb'=>'POST'),
           ),
        ),
    .......
);
###  We must make sure to configure the database as well. We can use the following database configuration:

/protected/config/database.php
--------------------------
// This is the database connection configuration.
return array(
   // uncomment the following lines to use a MySQL database
   'connectionString' => 'mysql:host=localhost;dbname=dbname',
   'emulatePrepare' => true,
   'username' => '',
   'password' => '',
   'charset' => 'utf8',
);

### Next Step we must to 'generate' the database and file upload directory. We can execute by url that will generate all the module needed:
`http://localhost/[project_name]/index.php/PXSocialMedia/generate`

#### API
`POST` `http://localhost/[project_name]/index.php/PXSocialMedia/api/login`

`Desc`
This API will save the user profile data from app to server

`Parameters`
username
password
email 
registration_type
social_id
avatar

`GET` `http://localhost/[project_name]/index.php/PXSocialMedia/api/registrationtype_list`

`Desc`
Listed all of supported social media

#### CRUD/User managemen
http://localhost/[project_name]/index.php/PXSocialMedia/manage